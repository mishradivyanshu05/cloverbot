#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask, request, render_template,redirect,jsonify
import requests,os,json,random
import sqlalchemy
import logging
from fuzzywuzzy import fuzz,process
from time import sleep

from requests_toolbelt.adapters import appengine
appengine.monkeypatch()
##conn = mysql.connector.connect(
##  host="localhost",
##  database = "clover",
##  user="root",
##  passwd="5october"
##)
PAGE_ACCESS_TOKEN = "EAAebKKNlnAoBAH4LrcK5TWX8AYnyw6xGq88dxTp5uPzUHhoWPdDwPQNtiz9RNf7Uqhbz79sH882VOUhYkL50EEFXLHj5cuJJMx7A93ZCzf9xMnSoorzmZBuGDu5drA76Ik3ATzCgp8wr5YVxCf0CNlMjdOBl46ZArtMVo0aZCFgu6yZBnoiGV"
db_user = 'root'
db_pass = '5october'
db_name = 'clover'
cloud_sql_connection_name = "stalwart-coast-235609:asia-south1:cloversql"
print(cloud_sql_connection_name)
app = Flask(__name__)

logger = logging.getLogger()

db = sqlalchemy.create_engine(
    # Equivalent URL:
    # mysql+pymysql://<db_user>:<db_pass>@/<db_name>?unix_socket=/cloudsql/<cloud_sql_instance_name>
    sqlalchemy.engine.url.URL(
        drivername='mysql+pymysql',
        username=db_user,
        password=db_pass,
        database=db_name,
        query={
            'unix_socket': '/cloudsql/{}'.format(cloud_sql_connection_name)
        }
    ),
    pool_size=5,
    max_overflow=2,
    pool_timeout=30,  # 30 seconds
    pool_recycle=1800,  # 30 minutes
)

@app.before_first_request
def create_tables():
    print("Creating Merchant")
    # Create tables (if they don't already exist)
    with db.connect() as conn:
        conn.execute(
            '''CREATE TABLE IF NOT EXISTS merchant(
            id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
            name varchar (100),
            phoneNumber varchar (15),
            isBillable BOOLEAN,
            client_id varchar(20) NOT NULL,
            code varchar(100) NOT NULL,
            merchant_id varchar(20) NOT NULL UNIQUE,
            employee_id varchar(30) NOT NULL,
            access_token varchar(100) NOT NULL)'''
        )
    print("Creating Product")
    with db.connect() as conn:
        conn.execute(
            '''CREATE TABLE IF NOT EXISTS products(
            id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
            merchant_id int(10),
            product_id varchar(20) NOT NULL UNIQUE,
            product_name varchar(200) NOT NULL,
            price varchar(20) NOT NULL,
            price_type varchar(10),
            defaultTaxRates BOOLEAN,
            cost DECIMAL(10,3),
            FOREIGN KEY(merchant_id) REFERENCES merchant(id))'''
        )
@app.route('/webhook', methods=['POST'])
def webhook():
    #return "ok",200
    print "post"
    data = request.get_json()
    #print data
    if data["object"] == "page":
        for entry in data["entry"]:
            for messaging_event in entry["messaging"]:
                if messaging_event.get("message"):
                    sender_id = messaging_event["sender"]["id"]
                    send_message(sender_id,'Sender Actions','saction')
                    sleep(.3)
                    try:
                        fname = messaging_event["message"]["attachments"][0]["title"]
                        ylink = messaging_event["message"]["attachments"][0]["url"]
                    except:
                        fname = None
                        ylink = None
                    try:
                        sticker_id = messaging_event["message"]["attachments"][0]["payload"]["sticker_id"]
                        print sticker_id
                    except:
                        print "no sticker"
                        sticker_id = None
                    try:
                        nlp = messaging_event["message"]["nlp"]["entities"]
                        nlpkeys = nlp.keys()
                        for key in nlpkeys:
                            if key=='greetings':
                                print "greeted"
                                confidence = nlp["greetings"][0]["confidence"]
                                if confidence > 0.999:
                                    print "I am greeted"
                                    params = {
                                        "access_token": PAGE_ACCESS_TOKEN
                                    }
                                    req = requests.get("https://graph.facebook.com/v2.6/%s?fields=first_name" %(sender_id), params=params)
                                    print req.content
                                    dat = json.loads(req.content)
                                    firstn = dat["first_name"]
                                    send_message(sender_id,'Hi %s, Welcome to CloverBot.\nYou can start searching for a item details by the name . \nExample "What is the price of bat? \n "what is the cost of ball" \n "show bats in range of rs500 to rs2500"' %firstn)
                                    return "ok"
                                else:
                                    print "not enough confidence"
                            elif key=='thanks':
                                confidence = nlp["thanks"][0]["confidence"]
                                if confidence > 0.989:
                                    print "thanked"
                                    params = {
                                        "access_token": PAGE_ACCESS_TOKEN
                                    }
                                    req = requests.get("https://graph.facebook.com/v2.6/%s?fields=first_name" %(sender_id), params=params)
                                    print req.content
                                    dat = json.loads(req.content)
                                    firstn = dat["first_name"]
                                    send_message(sender_id,'Your Welcome %s \nAlways at your service.' %firstn)
                                    send_message(sender_id,'If you liked it you can rate ( leave feedback for ) the bot. 😁 ')
                                    #send_message(sender_id,'Done'+' 😊')
                                    return "ok"
                                else:
                                    print "not confident"
                    except Exception as e:
                        print 'nlp exception', e
                        print "no nlp detected"
                    recipient_id = messaging_event["recipient"]["id"]  # the recipient's ID, which should be your page's facebook ID
                    try:
                        message_text = messaging_event["message"]["text"].lower().encode('utf-8')  # the message's text
                        if message_text.lower() == 'ok':
                            send_message(sender_id,'😇')
                            return "ok"
                        witai(sender_id,message_text)
                    except KeyError as e:
                        if sticker_id in (369239263222822,369239343222814,369239383222810,1652591134767287):
                            tnxmsg = ['Thanks for the thumbs up!, makes me feel good :)',
                                       "Thank you so much, you're nice :)",
                                       'Oh! Thanks',
                                       'Thank you, you can leave feedback about me from the manage option',
                                       '😇',
                                       '👍',
                                       'Thanks, Any suggestions for me?. You can write to mishradivyanshu05@gmail.com']
                            send_message(sender_id,random.choice(tnxmsg))
                            return "ok"
                        elif ylink:
                            send_message(sender_id, 'Detected Youtube link \nPlease Wait...')
                            youtubeinmp3(sender_id,ylink,fname)
                            send_message(sender_id,'Done'+' 😊')
                            return "ok"
                        else:
                            send_message(sender_id,"I don't recognize pictures or media, sorry 😵")
                            return "ok"
                        send_message(sender_id,"Not a text query, try again.")
                        return "ok"
                        break
                    except UnicodeEncodeError as e:
                        print 'actual message is '
                        print e
                        confusemsg = ['Encoding error for query ']
                        send_message(sender_id,random.choice(confusemsg)+message_text)
                        return "ok"
                        break
                    except Exception as e:
                        print "error is%s " %e
    send_message(sender_id,'Search Completed'+' :)')
    return "ok",200





def execute_query(data):
    try:
        headers = {'accept': 'application/json'}
        get_single_merchant_url = 'https://apisandbox.dev.clover.com/v3/merchants/%s?expand=items&access_token=%s'%(data[0],data[4])
        merchant_data = requests.get(get_single_merchant_url,headers=headers)
        m_data = json.loads(merchant_data.content)
        #print "merchant data",m_data
        m_name,m_phone,m_isbillable = m_data["name"],m_data["phoneNumber"],m_data["isBillable"]
        items = m_data["items"]["elements"]
        with db.connect() as conn:
            conn.execute('''insert into merchant(merchant_id,employee_id,client_id,code,access_token,name,phoneNumber,isBillable) values (%s,%s,%s,%s,%s,%s,%s,%s) ON DUPLICATE KEY UPDATE access_token = %s,name = %s,phoneNumber = %s''',(data[0],data[1],data[2],data[3],data[4],m_name,m_phone,m_isbillable,data[4],m_name,m_phone,))
            #return "Merchant Products and Orders Import successfull."
    except Exception as e:
        print (e)
        return "Welcome Back Your Items and orders will be updated if any",None
    try:
        for i in items:
            print "in item loop"
            item_insert_query = '''insert into products(product_id,product_name,price,price_type,defaultTaxRates,cost,merchant_id) select %s,%s,%s,%s,%s,%s,id from merchant where merchant_id=%s ON DUPLICATE KEY UPDATE price = %s,product_name = %s'''
            with db.connect() as conn:
                conn.execute(item_insert_query,(i['id'],i['name'],i['price'],i['priceType'],i['defaultTaxRates'],i['cost'],data[0],i['price'],i['name'],))
        return "Merchant Products and Orders Import successfull.",m_data
    except Exception as e:
        print e
        return "Welcome Back Your Items and orders will be updated if any",None

@app.route('/', methods=['GET'])
def run():
    print "start"
    url = "https://sandbox.dev.clover.com/oauth/authorize?client_id=ZC29J2AYDNX0J"
    return redirect(url)

@app.route('/postback', methods=['GET'])
def postback():
    merchant_id = request.args.get('merchant_id')
    employee_id = request.args.get('employee_id')
    client_id = request.args.get('client_id')
    code = request.args.get('code')
    url = 'https://sandbox.dev.clover.com/oauth/token?client_id=ZC29J2AYDNX0J&client_secret=81253bf9-7c86-4411-b4f5-71c455049867&code=%s'%code
    create_tables()
    r = requests.get(url)
    r = json.loads(r.content)
    access_token = r['access_token']
    msg,mdata = execute_query((str(merchant_id),str(employee_id),str(client_id),str(code),str(access_token)))
    if mdata == None:
        mdata={'items':{'elements':[]}}
    data = {"merchant_id":merchant_id,"employee_id":employee_id,"client_id":client_id,"code":code}
    return render_template('index.html',items=mdata['items']['elements'],msg={"msg":msg,"mdata":mdata})

def send_message(recipient_id, message_text,adlink='',fname='',imgurl='',clovermsg=''):
    params = {
        "access_token": PAGE_ACCESS_TOKEN
    }
    headers = {
        "Content-Type": "application/json"
    }
    if adlink=='saction':
        print "in elif"
        data = json.dumps({
        "recipient": {
        "id": recipient_id
        },
        "sender_action":"typing_on"
        })
    elif clovermsg=='yes':
        elemnts = []
        for i in message_text:
            data={}
            data['buttons']=[]
            price = i['price']
            product_name = i['product_name']
            merchant_name = i['merchant_name']
            data['title'] = product_name
            data['subtitle'] = merchant_name
            data['buttons'].append({"type":"web_url","url":"https://example.com","title":"Buy for %s"%price})
            elemnts.append(data)
            print data
        data = json.dumps({
        "recipient": {
        "id": recipient_id
        },
        "message":{
        "attachment":{
        "type":"template",
        "payload":{
        "template_type":"generic",
        "elements":elemnts
        }
        }
        }
        })
        print data
    else:
        data = json.dumps({
        "recipient": {
        "id": recipient_id
        },
        "message": {
        "text": message_text
        }
        })
    r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=params, headers=headers, data=data)
    if r.status_code != 200:
        print(r.status_code)
        print(r.text)

def searchdb(query):
    with db.connect() as conn:
        ress = conn.execute('''select merchant_id,product_name,price from products''')
    # conn = connection()
    # c = conn.cursor()
    # c.execute('''select merchant_id,product_name,price from products''')

        p_columns = ['merchant_id','product_name','price']
        p_list = [dict(zip(p_columns, row)) for row in ress.fetchall()]
    to_send=[]
    for item in p_list:
        slis=[]
        slis.append(item['product_name'])
        print item['product_name']
        res = process.extract(query,slis)
        print res[0][1]
        if res[0][1]>75:
            to_send.append(item)
    for i in to_send:
        print i
        with db.connect() as conn:
            ress = conn.execute('''select name from merchant where id=%s''',(i['merchant_id'],))
        #c.execute('''select name from merchant where id=%s''',(i['merchant_id'],))
            m_name = ress.fetchone()
            print m_name
            m_name = m_name[0]
            i['merchant_name'] = m_name
    return to_send

def witai(sender_id,message_text):
    witurl = 'https://api.wit.ai/message?v=20170307&q=%s'%message_text
    headers = {'Authorization': 'Bearer OSH6SFDPSSDFAYXLDZLK7ISBCMIQSPRJ'}
    r = requests.get(witurl,headers=headers)
    res = r.json()
    print res
    try:
        #print 'entites',res['entities']
        entities = res['entities']
        for key in entities.keys():
            print "1"
            if key == 'intent':
                print "2"
                if entities['intent'][0]['confidence'] > 0.70 and entities['intent'][0]['value']=='find_price':
                    print "3"
                    if 'search_query' in entities.keys():
                        print "4"
                        if entities['search_query'][0]['confidence'] > 0.92:
                            query = entities['search_query'][0]['value']
                            resp = searchdb(query)
                            send_message(sender_id,resp,clovermsg="yes")
                        else:
                            print ("Not enough search query confidence")
                    else:
                        print("no search query")
                else:
                    print ("not enough intent confidence")
    except Exception as e:
        print ("exception",str(e))
        pass


@app.route('/webhook', methods=['GET'])
def verify():
    print "indside verifiy func in get method"
    os.environ["VERIFY_TOKEN"] = "iloveyoulikeafatladylovesapple"
    if request.args.get("hub.mode") == "subscribe" and request.args.get("hub.challenge"):
        if not request.args.get("hub.verify_token") == os.environ["VERIFY_TOKEN"]:
            return "Verification token mismatch", 403
        return request.args["hub.challenge"], 200

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080, debug=True)
